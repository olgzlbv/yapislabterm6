package core;

import gen.setLanguageParser;
import org.antlr.v4.runtime.ParserRuleContext;

import javax.annotation.Nullable;

public enum SetOperations {

    PLUS(setLanguageParser.MUL, "Set.union(%s,%s)"),
    MINUS(setLanguageParser.SUB, "Set.diff(%s,%s)"),
    DIVIDE(setLanguageParser.DIV, "Set.simDiff(%s,%s)"),
    MULTIPLY(setLanguageParser.MUL, "Set.intersection(%s,%s)"),;

    private final int type;
    private final String overrideSet;

    SetOperations(int type, String overrideSet) {
        this.type = type;
        this.overrideSet = overrideSet;
    }

    @Nullable
    public static SetOperations findOperation(ParserRuleContext ctx) {
        for (SetOperations numberOperations : SetOperations.values()) {
            if (ctx.getToken(numberOperations.type, 0) != null) {
                return numberOperations;
            }
        }
        return null;
    }

    public String getOverrideSet() {
        return overrideSet;
    }

}
