package core;

import com.google.common.base.Preconditions;
import core.methods.Method;
import core.methods.MethodType;
import core.variables.Variable;
import core.variables.VariableType;
import gen.setLanguageParser;
import gen.setLanguageVisitor;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class setLanguageVisitorInherited implements setLanguageVisitor<String> {

    private final setLanguageParser parserV1;
    private final Memory memory = new Memory();
    private final String name;

    @Override
    public String visit(ParseTree parseTree) {
        String out = parseTree.accept(this);
        System.out.println(out);
        return out;
    }

    @Override
    public String visitChildren(RuleNode ruleNode) {
        return null;
    }

    @Override
    public String visitTerminal(TerminalNode terminalNode) {
        return null;
    }

    @Override
    public String visitErrorNode(ErrorNode errorNode) {
        return null;
    }

    @Override
    public String visitGlobal_program(setLanguageParser.Global_programContext ctx) {
        StringBuilder out = new StringBuilder();
        out.append("" +
                "public class "+ name + " {\n" +
                "public static void main (String args[]){\n" +
                "try{\n" +
                "main_program();" +
                "} catch (Exception ex){" +
                "ex.printStackTrace();" +
                "}\n" +
                "}\n");
        memory.regMethod(new Method("main_program", MethodType.RETURN_OPTIONAL, Collections.emptyList()));
        List<setLanguageParser.Subprogram_non_returnContext> non_returnContexts = ctx.subprogram_non_return();
        List<setLanguageParser.Subprogram_returnContext> returnContexts = ctx.subprogram_return();
        for (setLanguageParser.Global_assign_varContext ct : ctx.global_assign_var()) {
            out.append(ct.accept(this));
        }
        for (setLanguageParser.Global_assign_setContext ct : ctx.global_assign_set()) {
            out.append(ct.accept(this));
        }
        for (setLanguageParser.Subprogram_returnContext ct : returnContexts) {
            VariableType variableType = Preconditions.checkNotNull(VariableType.findByDisplayName(ct.type().getText()));
            memory.regMethod(new Method(ct.ID().toString(), Preconditions.checkNotNull(MethodType.findByReturnedType(variableType)), collectMethodArguments(ct.signature())));
            out.append(ct.accept(this));
        }
        for (setLanguageParser.Subprogram_non_returnContext ct : non_returnContexts) {
            memory.regMethod(new Method(ct.ID().toString(), MethodType.RETURN_OPTIONAL, collectMethodArguments(ct.signature())));
            out.append(ct.accept(this));
        }
        out.append(ctx.program().accept(this));
        out.append("\n" + "}" + "\n");
        return out.toString();
    }


    public setLanguageVisitorInherited(setLanguageParser parserV1, String name) {
        this.parserV1 = parserV1;
        this.name = name;
    }

    @Override
    public String visitProgram(setLanguageParser.ProgramContext ctx) {
        memory.registerMethodInvocation();
        String out = "private static void main_program () " + ctx.block().accept(this);
        memory.registerMethodInvocationEnded();
        return out;
    }

    @Override
    public String visitBlock(setLanguageParser.BlockContext ctx) {
        memory.registerNew​​VisibilityArea();
        StringBuilder out = new StringBuilder("{\n").append(invokeAllStatementsForBlock(ctx.statement()));
        out.append("}\n");
        memory.registerVisibilityAreaEnded();
        return out.toString();
    }

    private StringBuilder invokeAllStatementsForBlock(List<setLanguageParser.StatementContext> list) {
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            out.append(list.get(i).accept(this));
        }
        return out;
    }

    @Override
    public String visitStatement(setLanguageParser.StatementContext ctx) {
        Statement statement = Statement.findStatement(ctx);
        Preconditions.checkNotNull(statement);
        return statement.invoke(ctx, this);
    }

    @Override
    public String visitDigit_expression(setLanguageParser.Digit_expressionContext ctx) {
        validateDigitExpression(ctx);
        return concatExpr(ctx) + ";\n";
    }

    @Override
    public String visitIntialize_set(setLanguageParser.Intialize_setContext ctx) {
        String ID = ((setLanguageParser.Assign_setContext) ctx.parent).ID().getText();
        return VariableType.SET.getOutName() + " " + ID + "=" + "new Set()" + ";\n" + handleSetInitialization(ctx.ID(), ID);
    }

    private String handleSetInitialization(List<TerminalNode> list, String rootID) {
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            Variable variable = Preconditions.checkNotNull(memory.getVariable(list.get(i).getText()));
            if (variable.getVariableType() != VariableType.ELEMENT) {
                throw new UnsupportedOperationException();
            }
            out.append(rootID).append(".").append(String.format("add(%s)", variable.getID())).append(";\n");
        }
        return out.toString();
    }

    @Override
    public String visitIntialize_element(setLanguageParser.Intialize_elementContext ctx) {
        return String.format("new Element(%s)", ctx.STRING()) + ";\n";
    }

    @Override
    public String visitAssign_var_method_invocation(setLanguageParser.Assign_var_method_invocationContext ctx) {
        Variable variable = new Variable(ctx.ID().getText(), Preconditions.checkNotNull(VariableType.findByDisplayName(ctx.type().getText())), ctx.CONST() != null);
        Preconditions.checkState(memory.registerVariable(variable));
        Method method = Preconditions.checkNotNull(memory.getRegisteredMethod(ctx.method_invokation().ID().getText()));
        if (method.getMethodType() == MethodType.RETURN_OPTIONAL || method.getMethodType().getReturnedType() != variable.getVariableType()) {
            throw new UnsupportedOperationException();
        }
        return variable.getVariableType().getOutName() + " " + variable.getID() + "=" + ctx.method_invokation().accept(this);
    }

    private void validateDigitExpression(setLanguageParser.Digit_expressionContext ctx) {
        if (ctx.ID() != null) {
            Variable variable = memory.getVariable(ctx.ID().toString());
            if (variable == null || variable.getVariableType() != VariableType.INT) {
                throw new UnsupportedOperationException();
            }
        }
        List<setLanguageParser.Digit_expressionContext> expr = ctx.digit_expression();
        for (int i = 0; i < expr.size(); i++) {
            validateDigitExpression(expr.get(i));
        }
    }

    @Override
    public String visitAssign_var(setLanguageParser.Assign_varContext ctx) {
        return processAssignVar(ctx, false);
    }

    private String processAssignVar(setLanguageParser.Assign_varContext ctx, boolean global) {
        VariableType variableType = Preconditions.checkNotNull(VariableType.findByDisplayName(ctx.type_1().getText()));
        String out = variableType.getOutName() + " " + ctx.ID() + "=" + variableType.invokeInitLine(ctx, this);
            if (!global) {
            Preconditions.checkState(memory.registerVariable(new Variable(ctx.ID().toString(), variableType, ctx.CONST() != null)));
        } else {
            Preconditions.checkState(memory.registerGlobalVariable(new Variable(ctx.ID().toString(), variableType, ctx.CONST() != null)));
        }
        return out;
    }

    @Override
    public String visitAssign_set(setLanguageParser.Assign_setContext ctx) {
        return processAssignSet(ctx, false);
    }

    private String processAssignSet(setLanguageParser.Assign_setContext ctx, boolean global) {
        StringBuilder out = new StringBuilder();
        SetOperations setOperations = SetOperations.findOperation(ctx.intialize_set());
        Variable varForRegister;
        if (setOperations == null) {
            out.append(VariableType.SET.invokeInitLine(ctx, this));
            varForRegister = new Variable(ctx.ID().toString(), VariableType.SET, ctx.CONST() != null);
        } else {
            for (int i = 1; i < 2; i++) {
                Variable variable = Preconditions.checkNotNull(memory.getVariable(ctx.intialize_set().ID(i).getText()));
                if (variable.getVariableType() != VariableType.SET) {
                    throw new UnsupportedOperationException();
                }
            }
            out.append(VariableType.SET.getOutName()).append(" ").append(ctx.ID()).append("=");
            out.append(String.format(setOperations.getOverrideSet(), ctx.intialize_set().ID(0), ctx.intialize_set().ID(1))).append(";\n");
            varForRegister = new Variable(ctx.ID().toString(), VariableType.SET);
        }
        if (!global) {
            Preconditions.checkState(memory.registerVariable(varForRegister));
        } else {
            Preconditions.checkState(memory.registerGlobalVariable(varForRegister));
        }
        return out.toString();
    }


    @Override
    public String visitPrint(setLanguageParser.PrintContext ctx) {
        if (ctx.ID() != null) {
            Variable variable = Preconditions.checkNotNull(memory.getVariable(ctx.ID().getText()));
            return String.format("System.out.println(String.valueOf(%s))", variable.getID()) + ";\n";
        } else {
            return String.format("System.out.println(String.valueOf(%s))", ctx.STRING() == null ? ctx.NUMBER() : ctx.STRING()) + ";\n";
        }
    }

    @Override
    public String visitOperations(setLanguageParser.OperationsContext ctx) {
        Variable variable = memory.getVariable(ctx.ID().toString());
        if (variable == null || variable.getVariableType() != VariableType.INT || variable.isConstant()) {
            throw new UnsupportedOperationException();
        }
        validateDigitExpression(ctx.digit_expression());
        return variable.getID() + "=" + concatExpr(ctx.digit_expression()) + ";\n";
    }

    @Override
    //TODO rework.
    public String visitOperarions_with_set(setLanguageParser.Operarions_with_setContext ctx) {
        Variable variable = Preconditions.checkNotNull(memory.getVariable(ctx.ID(0).getText()));
        Variable el = Preconditions.checkNotNull(memory.getVariable(ctx.ID(1).getText()));
        if (variable.getVariableType() != VariableType.SET || el.getVariableType() != VariableType.ELEMENT || variable.isConstant()) {
            throw new UnsupportedOperationException();
        }
        if (ctx.ADD() == null) {
            return variable.getID() + "." + String.format("remove(%s)", el.getID()) + ";\n";
        } else {
            return variable.getID() + "." + String.format("remove(%s)", el.getID()) + ";\n";
        }
    }

    @Override
    public String visitSimple_compare(setLanguageParser.Simple_compareContext ctx) {
        List<setLanguageParser.Digit_expressionContext> dctx = ctx.digit_expression();
        validateDigitExpression(dctx.get(0));
        validateDigitExpression(dctx.get(1));
        return concatExpr(ctx);
    }

    private String concatExpr(ParserRuleContext ctx) {
        StringBuilder out = new StringBuilder();
        List<Token> tokens = parserV1.getTokens(ctx.start, ctx.stop);
        for (int i = 0; i < tokens.size(); i++) {
            out.append(tokens.get(i).getText());
        }
        return out.toString();
    }

    @Override
    public String visitHard_compare(setLanguageParser.Hard_compareContext ctx) {
        return ctx.NEGATION().getText() + ctx.O_BRACKET().getText() + visitSimple_compare(ctx.simple_compare()) + ctx.C_BRACKET().getText();
    }

    @Override
    public String visitWhile_cicle(setLanguageParser.While_cicleContext ctx) {
        String out = String.format("while (%s)", handlerCompare(ctx.hard_compare(), ctx.simple_compare()));
        out += ctx.block().accept(this);
        return out;
    }

    private String handlerCompare(setLanguageParser.Hard_compareContext htx, setLanguageParser.Simple_compareContext stx) {
        return stx == null ? htx.accept(this) : stx.accept(this);
    }

    @Override
    public String visitIf_then(setLanguageParser.If_thenContext ctx) {
        return String.format("if (%s) %s else %s", handlerCompare(ctx.hard_compare(), ctx.simple_compare()), ctx.block(0).accept(this), ctx.block(1).accept(this));
    }

    @Override
    public String visitFor_each(setLanguageParser.For_eachContext ctx) {
        Variable variable = memory.getVariable(ctx.ID(1).getText());
        if (variable == null || variable.getVariableType() != VariableType.SET) {
            throw new UnsupportedOperationException();
        }
        Preconditions.checkState(memory.registerVariable(new Variable(ctx.ID(0).getText(), VariableType.ELEMENT)));
        return String.format("for (%s:%s)", "Element" + " " + ctx.ID(0).getText(), variable.getID()) + ctx.block().accept(this);
    }

    @Override
    public String visitGlobal_assign_set(setLanguageParser.Global_assign_setContext ctx) {
        return "private static " + processAssignSet(ctx.assign_set(), true);
    }

    @Override
    public String visitGlobal_assign_var(setLanguageParser.Global_assign_varContext ctx) {
        return "private static " + processAssignVar(ctx.assign_var(), true);
    }

    @Override
    public String visitType(setLanguageParser.TypeContext ctx) {
        VariableType variableType = VariableType.findByDisplayName(ctx.getText());
        return Preconditions.checkNotNull(variableType).getOutName();
    }

    @Override
    public String visitType_1(setLanguageParser.Type_1Context ctx) {
        VariableType variableType = VariableType.findByDisplayName(ctx.getText());
        return Preconditions.checkNotNull(variableType).getOutName();
    }

    @Override
    public String visitSignature(setLanguageParser.SignatureContext ctx) {
        StringBuilder builder = new StringBuilder();
        builder.append("(");
        for (int i = 0; i < ctx.ID().size(); i++) {
            VariableType variableType = VariableType.findByDisplayName(ctx.type(i).getText());
            Preconditions.checkNotNull(variableType);
            memory.registerVariable(new Variable(ctx.ID(i).getText(), variableType));
            builder.append(ctx.type(i).accept(this)).append(" ").append(ctx.ID(i));
            if (i != ctx.ID().size() - 1) {
                builder.append(",");
            }
        }
        builder.append(")");
        return builder.toString();
    }

    @Override
    public String visitSubprogram_return(setLanguageParser.Subprogram_returnContext ctx) {
        Method method = memory.getRegisteredMethod(ctx.ID().toString());
        if (method == null || method.getMethodType() == MethodType.RETURN_OPTIONAL) {
            throw new UnsupportedOperationException();
        }
        memory.registerMethodInvocation();
        //NPE checked before.
        String s = "private static " + method.getMethodType().getReturnedType().getOutName()
                + " " + ctx.ID() + ctx.signature().accept(this) + ctx.block_return().accept(this);
        memory.registerMethodInvocationEnded();
        return s;
    }

    @Override
    public String visitSubprogram_non_return(setLanguageParser.Subprogram_non_returnContext ctx) {
        Method method = memory.getRegisteredMethod(ctx.ID().toString());
        if (method == null || method.getMethodType() != MethodType.RETURN_OPTIONAL) {
            throw new UnsupportedOperationException();
        }
        memory.registerMethodInvocation();
        String s = "private static " + "void" + " " + ctx.ID() + handleSignature(ctx.signature());
        s += ctx.block_non_return() == null ? ctx.block().accept(this) : ctx.block_non_return().accept(this);
        memory.registerMethodInvocationEnded();
        return s;
    }

    private String handleSignature(setLanguageParser.SignatureContext ctx) {
        return ctx == null ? "(" + ")" : ctx.accept(this);
    }

    @Override
    public String visitBlock_return(setLanguageParser.Block_returnContext ctx) {
        memory.registerNew​​VisibilityArea();
        Method method = Preconditions.checkNotNull(memory.getRegisteredMethod(((setLanguageParser.Subprogram_returnContext) ctx.parent).ID().getText()));
        StringBuilder out = new StringBuilder("{\n").append(invokeAllStatementsForBlock(ctx.statement()));
        out.append("return");
        Variable variable = Preconditions.checkNotNull(memory.getVariable(ctx.ID().getText()));
        if (variable.getVariableType() != method.getMethodType().getReturnedType()) {
            throw new UnsupportedOperationException();
        }
        out.append(" ").append(variable.getID()).append(";\n").append("}\n");
        memory.registerVisibilityAreaEnded();
        return out.toString();
    }

    @Override
    public String visitBlock_non_return(setLanguageParser.Block_non_returnContext ctx) {
        memory.registerNew​​VisibilityArea();
        StringBuilder out = new StringBuilder("{\n").append(invokeAllStatementsForBlock(ctx.statement()));
        out.append("return");
        out.append(";\n");
        out.append("}\n");
        memory.registerVisibilityAreaEnded();
        return out.toString();
    }

    @Override
    public String visitSignature_method_invokation(setLanguageParser.Signature_method_invokationContext ctx) {
        StringBuilder builder = new StringBuilder();
        builder.append("(");
        Method method = memory.getRegisteredMethod(((setLanguageParser.Method_invokationContext) ctx.parent).ID().getText());
        Preconditions.checkNotNull(method);
        if (method.getArguments().size() != ctx.ID().size()) {
            throw new UnsupportedOperationException();
        }
        for (int i = 0; i < ctx.ID().size(); i++) {
            Variable variable = memory.getVariable(ctx.ID(i).getText());
            if (variable == null || method.getArguments().get(i) != variable.getVariableType()) {
                throw new UnsupportedOperationException();
            }
            builder.append(variable.getID());
        }
        return builder.append(")").toString();
    }

    @Override
    public String visitMethod_invokation(setLanguageParser.Method_invokationContext ctx) {
        Method method = memory.getRegisteredMethod(ctx.ID().toString());
        if (method == null) {
            throw new UnsupportedOperationException();
        }
        return method.getID() + " " + handleSignatureOfInvocation(ctx) + ";\n";
    }

    private String handleSignatureOfInvocation(setLanguageParser.Method_invokationContext ctx) {
        if (ctx.signature_method_invokation() == null) {
            Method method = Preconditions.checkNotNull(memory.getRegisteredMethod(ctx.ID().getText()));
            if (method.getArguments().size() != 0) {
                throw new UnsupportedOperationException();
            }
            return "(" + ")";
        }
        return ctx.signature_method_invokation().accept(this);
    }


    private List<VariableType> collectMethodArguments(setLanguageParser.SignatureContext ctx) {
        List<VariableType> variableTypes = new ArrayList<>();
        if (ctx == null) {
            return Collections.emptyList();
        }
        for (int i = 0; i < ctx.ID().size(); i++) {
            VariableType variableType = VariableType.findByDisplayName(ctx.type(i).getText());
            variableTypes.add(Preconditions.checkNotNull(variableType));
        }
        return variableTypes;
    }


}
