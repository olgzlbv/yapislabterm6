import core.setLanguageVisitorInherited;
import gen.setLanguageLexer;
import gen.setLanguageParser;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.FileWriter;

public class Main {

    public static void main(String args[]) {
        try {
            setLanguageLexer lexer = new setLanguageLexer(new ANTLRFileStream("set.mn"));
            setLanguageParser parser = new setLanguageParser(new CommonTokenStream(lexer));
            ParseTree tree = parser.global_program();
            setLanguageVisitorInherited listVisitor = new setLanguageVisitorInherited(parser, "Main");
            String output = listVisitor.visit(tree);
            FileWriter fileWriter = new FileWriter("generated/Main.java");
            fileWriter.write(output);
            fileWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
