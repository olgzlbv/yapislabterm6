package gen;// Generated from D:/Programs/IntelliJ IDEA 2019.3.4/proj/setCompiler\setLanguage.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link setLanguageParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface setLanguageVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(setLanguageParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(setLanguageParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(setLanguageParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#global_program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGlobal_program(setLanguageParser.Global_programContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#digit_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDigit_expression(setLanguageParser.Digit_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#intialize_set}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntialize_set(setLanguageParser.Intialize_setContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#intialize_element}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntialize_element(setLanguageParser.Intialize_elementContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#assign_var_method_invocation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign_var_method_invocation(setLanguageParser.Assign_var_method_invocationContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#assign_set}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign_set(setLanguageParser.Assign_setContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#assign_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign_var(setLanguageParser.Assign_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(setLanguageParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#operations}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperations(setLanguageParser.OperationsContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#operarions_with_set}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperarions_with_set(setLanguageParser.Operarions_with_setContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#simple_compare}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimple_compare(setLanguageParser.Simple_compareContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#hard_compare}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHard_compare(setLanguageParser.Hard_compareContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#while_cicle}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile_cicle(setLanguageParser.While_cicleContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#if_then}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_then(setLanguageParser.If_thenContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#for_each}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_each(setLanguageParser.For_eachContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#global_assign_set}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGlobal_assign_set(setLanguageParser.Global_assign_setContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#global_assign_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGlobal_assign_var(setLanguageParser.Global_assign_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(setLanguageParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#type_1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_1(setLanguageParser.Type_1Context ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#signature}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSignature(setLanguageParser.SignatureContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#subprogram_return}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubprogram_return(setLanguageParser.Subprogram_returnContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#subprogram_non_return}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubprogram_non_return(setLanguageParser.Subprogram_non_returnContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#block_return}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock_return(setLanguageParser.Block_returnContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#block_non_return}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock_non_return(setLanguageParser.Block_non_returnContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#signature_method_invokation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSignature_method_invokation(setLanguageParser.Signature_method_invokationContext ctx);
	/**
	 * Visit a parse tree produced by {@link setLanguageParser#method_invokation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethod_invokation(setLanguageParser.Method_invokationContext ctx);
}