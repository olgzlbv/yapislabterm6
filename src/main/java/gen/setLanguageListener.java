package gen;// Generated from D:/Programs/IntelliJ IDEA 2019.3.4/proj/setCompiler\setLanguage.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link setLanguageParser}.
 */
public interface setLanguageListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(setLanguageParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(setLanguageParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(setLanguageParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(setLanguageParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(setLanguageParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(setLanguageParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#global_program}.
	 * @param ctx the parse tree
	 */
	void enterGlobal_program(setLanguageParser.Global_programContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#global_program}.
	 * @param ctx the parse tree
	 */
	void exitGlobal_program(setLanguageParser.Global_programContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#digit_expression}.
	 * @param ctx the parse tree
	 */
	void enterDigit_expression(setLanguageParser.Digit_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#digit_expression}.
	 * @param ctx the parse tree
	 */
	void exitDigit_expression(setLanguageParser.Digit_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#intialize_set}.
	 * @param ctx the parse tree
	 */
	void enterIntialize_set(setLanguageParser.Intialize_setContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#intialize_set}.
	 * @param ctx the parse tree
	 */
	void exitIntialize_set(setLanguageParser.Intialize_setContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#intialize_element}.
	 * @param ctx the parse tree
	 */
	void enterIntialize_element(setLanguageParser.Intialize_elementContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#intialize_element}.
	 * @param ctx the parse tree
	 */
	void exitIntialize_element(setLanguageParser.Intialize_elementContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#assign_var_method_invocation}.
	 * @param ctx the parse tree
	 */
	void enterAssign_var_method_invocation(setLanguageParser.Assign_var_method_invocationContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#assign_var_method_invocation}.
	 * @param ctx the parse tree
	 */
	void exitAssign_var_method_invocation(setLanguageParser.Assign_var_method_invocationContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#assign_set}.
	 * @param ctx the parse tree
	 */
	void enterAssign_set(setLanguageParser.Assign_setContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#assign_set}.
	 * @param ctx the parse tree
	 */
	void exitAssign_set(setLanguageParser.Assign_setContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#assign_var}.
	 * @param ctx the parse tree
	 */
	void enterAssign_var(setLanguageParser.Assign_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#assign_var}.
	 * @param ctx the parse tree
	 */
	void exitAssign_var(setLanguageParser.Assign_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(setLanguageParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(setLanguageParser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#operations}.
	 * @param ctx the parse tree
	 */
	void enterOperations(setLanguageParser.OperationsContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#operations}.
	 * @param ctx the parse tree
	 */
	void exitOperations(setLanguageParser.OperationsContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#operarions_with_set}.
	 * @param ctx the parse tree
	 */
	void enterOperarions_with_set(setLanguageParser.Operarions_with_setContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#operarions_with_set}.
	 * @param ctx the parse tree
	 */
	void exitOperarions_with_set(setLanguageParser.Operarions_with_setContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#simple_compare}.
	 * @param ctx the parse tree
	 */
	void enterSimple_compare(setLanguageParser.Simple_compareContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#simple_compare}.
	 * @param ctx the parse tree
	 */
	void exitSimple_compare(setLanguageParser.Simple_compareContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#hard_compare}.
	 * @param ctx the parse tree
	 */
	void enterHard_compare(setLanguageParser.Hard_compareContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#hard_compare}.
	 * @param ctx the parse tree
	 */
	void exitHard_compare(setLanguageParser.Hard_compareContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#while_cicle}.
	 * @param ctx the parse tree
	 */
	void enterWhile_cicle(setLanguageParser.While_cicleContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#while_cicle}.
	 * @param ctx the parse tree
	 */
	void exitWhile_cicle(setLanguageParser.While_cicleContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#if_then}.
	 * @param ctx the parse tree
	 */
	void enterIf_then(setLanguageParser.If_thenContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#if_then}.
	 * @param ctx the parse tree
	 */
	void exitIf_then(setLanguageParser.If_thenContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#for_each}.
	 * @param ctx the parse tree
	 */
	void enterFor_each(setLanguageParser.For_eachContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#for_each}.
	 * @param ctx the parse tree
	 */
	void exitFor_each(setLanguageParser.For_eachContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#global_assign_set}.
	 * @param ctx the parse tree
	 */
	void enterGlobal_assign_set(setLanguageParser.Global_assign_setContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#global_assign_set}.
	 * @param ctx the parse tree
	 */
	void exitGlobal_assign_set(setLanguageParser.Global_assign_setContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#global_assign_var}.
	 * @param ctx the parse tree
	 */
	void enterGlobal_assign_var(setLanguageParser.Global_assign_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#global_assign_var}.
	 * @param ctx the parse tree
	 */
	void exitGlobal_assign_var(setLanguageParser.Global_assign_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(setLanguageParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(setLanguageParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#type_1}.
	 * @param ctx the parse tree
	 */
	void enterType_1(setLanguageParser.Type_1Context ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#type_1}.
	 * @param ctx the parse tree
	 */
	void exitType_1(setLanguageParser.Type_1Context ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#signature}.
	 * @param ctx the parse tree
	 */
	void enterSignature(setLanguageParser.SignatureContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#signature}.
	 * @param ctx the parse tree
	 */
	void exitSignature(setLanguageParser.SignatureContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#subprogram_return}.
	 * @param ctx the parse tree
	 */
	void enterSubprogram_return(setLanguageParser.Subprogram_returnContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#subprogram_return}.
	 * @param ctx the parse tree
	 */
	void exitSubprogram_return(setLanguageParser.Subprogram_returnContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#subprogram_non_return}.
	 * @param ctx the parse tree
	 */
	void enterSubprogram_non_return(setLanguageParser.Subprogram_non_returnContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#subprogram_non_return}.
	 * @param ctx the parse tree
	 */
	void exitSubprogram_non_return(setLanguageParser.Subprogram_non_returnContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#block_return}.
	 * @param ctx the parse tree
	 */
	void enterBlock_return(setLanguageParser.Block_returnContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#block_return}.
	 * @param ctx the parse tree
	 */
	void exitBlock_return(setLanguageParser.Block_returnContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#block_non_return}.
	 * @param ctx the parse tree
	 */
	void enterBlock_non_return(setLanguageParser.Block_non_returnContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#block_non_return}.
	 * @param ctx the parse tree
	 */
	void exitBlock_non_return(setLanguageParser.Block_non_returnContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#signature_method_invokation}.
	 * @param ctx the parse tree
	 */
	void enterSignature_method_invokation(setLanguageParser.Signature_method_invokationContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#signature_method_invokation}.
	 * @param ctx the parse tree
	 */
	void exitSignature_method_invokation(setLanguageParser.Signature_method_invokationContext ctx);
	/**
	 * Enter a parse tree produced by {@link setLanguageParser#method_invokation}.
	 * @param ctx the parse tree
	 */
	void enterMethod_invokation(setLanguageParser.Method_invokationContext ctx);
	/**
	 * Exit a parse tree produced by {@link setLanguageParser#method_invokation}.
	 * @param ctx the parse tree
	 */
	void exitMethod_invokation(setLanguageParser.Method_invokationContext ctx);
}