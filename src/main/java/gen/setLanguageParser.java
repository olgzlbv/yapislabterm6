package gen;// Generated from D:/Programs/IntelliJ IDEA 2019.3.4/proj/setCompiler\setLanguage.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class setLanguageParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	public List<Token> getTokens(Token start, Token end) {
		List<Token> list = new ArrayList<>();
		for (int i = start.getTokenIndex(); i <= end.getTokenIndex(); i++) {
			list.add(getTokenStream().get(i));
		}
		return list;
	}
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, BEGIN=5, SET=6, ELEMENT=7, ADD=8, REMOVE=9, 
		END=10, PRINT=11, INT=12, WHILE=13, FOR=14, CONST=15, IF=16, THEN=17, 
		CALL=18, RETURN=19, SEPARATOR=20, DELIMITER=21, COLON=22, GLOBAL=23, ID=24, 
		STRING=25, NUMBER=26, WS=27, COMMENTS=28, ADDS=29, SUB=30, MUL=31, DIV=32, 
		NEGATION=33, EQUAL=34, NON_EQUAL=35, LESS=36, LESS_OR_EQUALS=37, GREATER=38, 
		GREATER_OR_EQUALS=39, O_BRACKET=40, C_BRACKET=41, K_O_BRACKET=42, K_C_BRACKET=43;
	public static final int
		RULE_program = 0, RULE_block = 1, RULE_statement = 2, RULE_global_program = 3, 
		RULE_digit_expression = 4, RULE_intialize_set = 5, RULE_intialize_element = 6, 
		RULE_assign_var_method_invocation = 7, RULE_assign_set = 8, RULE_assign_var = 9, 
		RULE_print = 10, RULE_operations = 11, RULE_operarions_with_set = 12, 
		RULE_simple_compare = 13, RULE_hard_compare = 14, RULE_while_cicle = 15, 
		RULE_if_then = 16, RULE_for_each = 17, RULE_global_assign_set = 18, RULE_global_assign_var = 19, 
		RULE_type = 20, RULE_type_1 = 21, RULE_signature = 22, RULE_subprogram_return = 23, 
		RULE_subprogram_non_return = 24, RULE_block_return = 25, RULE_block_non_return = 26, 
		RULE_signature_method_invokation = 27, RULE_method_invokation = 28;
	private static String[] makeRuleNames() {
		return new String[] {
			"program", "block", "statement", "global_program", "digit_expression", 
			"intialize_set", "intialize_element", "assign_var_method_invocation", 
			"assign_set", "assign_var", "print", "operations", "operarions_with_set", 
			"simple_compare", "hard_compare", "while_cicle", "if_then", "for_each", 
			"global_assign_set", "global_assign_var", "type", "type_1", "signature", 
			"subprogram_return", "subprogram_non_return", "block_return", "block_non_return", 
			"signature_method_invokation", "method_invokation"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'main_program'", "','", "'='", "'sub_program'", "'begin'", "'set'", 
			"'element'", "'add'", "'remove'", "'end'", "'print'", "'int'", "'while'", 
			"'for'", "'const'", "'if'", "'then'", "'call'", "'return'", "';'", "'.'", 
			"':'", "'global'", null, null, null, null, null, "'+'", "'-'", "'*'", 
			"'/'", "'!'", "'=='", "'!='", "'<'", "'<='", "'>'", "'>='", "'('", "')'", 
			"'{'", "'}'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, "BEGIN", "SET", "ELEMENT", "ADD", "REMOVE", 
			"END", "PRINT", "INT", "WHILE", "FOR", "CONST", "IF", "THEN", "CALL", 
			"RETURN", "SEPARATOR", "DELIMITER", "COLON", "GLOBAL", "ID", "STRING", 
			"NUMBER", "WS", "COMMENTS", "ADDS", "SUB", "MUL", "DIV", "NEGATION", 
			"EQUAL", "NON_EQUAL", "LESS", "LESS_OR_EQUALS", "GREATER", "GREATER_OR_EQUALS", 
			"O_BRACKET", "C_BRACKET", "K_O_BRACKET", "K_C_BRACKET"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "setLanguage.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public setLanguageParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgramContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(58);
			match(T__0);
			setState(59);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public TerminalNode BEGIN() { return getToken(setLanguageParser.BEGIN, 0); }
		public TerminalNode END() { return getToken(setLanguageParser.END, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(61);
			match(BEGIN);
			setState(65);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SET) | (1L << ELEMENT) | (1L << PRINT) | (1L << INT) | (1L << WHILE) | (1L << FOR) | (1L << IF) | (1L << CALL) | (1L << ID))) != 0)) {
				{
				{
				setState(62);
				statement();
				}
				}
				setState(67);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(68);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public Assign_varContext assign_var() {
			return getRuleContext(Assign_varContext.class,0);
		}
		public Assign_setContext assign_set() {
			return getRuleContext(Assign_setContext.class,0);
		}
		public OperationsContext operations() {
			return getRuleContext(OperationsContext.class,0);
		}
		public Assign_var_method_invocationContext assign_var_method_invocation() {
			return getRuleContext(Assign_var_method_invocationContext.class,0);
		}
		public While_cicleContext while_cicle() {
			return getRuleContext(While_cicleContext.class,0);
		}
		public If_thenContext if_then() {
			return getRuleContext(If_thenContext.class,0);
		}
		public Method_invokationContext method_invokation() {
			return getRuleContext(Method_invokationContext.class,0);
		}
		public PrintContext print() {
			return getRuleContext(PrintContext.class,0);
		}
		public Operarions_with_setContext operarions_with_set() {
			return getRuleContext(Operarions_with_setContext.class,0);
		}
		public For_eachContext for_each() {
			return getRuleContext(For_eachContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_statement);
		try {
			setState(80);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(70);
				assign_var();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(71);
				assign_set();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(72);
				operations();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(73);
				assign_var_method_invocation();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(74);
				while_cicle();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(75);
				if_then();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(76);
				method_invokation();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(77);
				print();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(78);
				operarions_with_set();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(79);
				for_each();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Global_programContext extends ParserRuleContext {
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public List<Subprogram_non_returnContext> subprogram_non_return() {
			return getRuleContexts(Subprogram_non_returnContext.class);
		}
		public Subprogram_non_returnContext subprogram_non_return(int i) {
			return getRuleContext(Subprogram_non_returnContext.class,i);
		}
		public List<Subprogram_returnContext> subprogram_return() {
			return getRuleContexts(Subprogram_returnContext.class);
		}
		public Subprogram_returnContext subprogram_return(int i) {
			return getRuleContext(Subprogram_returnContext.class,i);
		}
		public List<Global_assign_setContext> global_assign_set() {
			return getRuleContexts(Global_assign_setContext.class);
		}
		public Global_assign_setContext global_assign_set(int i) {
			return getRuleContext(Global_assign_setContext.class,i);
		}
		public List<Global_assign_varContext> global_assign_var() {
			return getRuleContexts(Global_assign_varContext.class);
		}
		public Global_assign_varContext global_assign_var(int i) {
			return getRuleContext(Global_assign_varContext.class,i);
		}
		public Global_programContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_global_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterGlobal_program(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitGlobal_program(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitGlobal_program(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Global_programContext global_program() throws RecognitionException {
		Global_programContext _localctx = new Global_programContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_global_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(86);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__3) {
				{
				setState(84);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
				case 1:
					{
					setState(82);
					subprogram_non_return();
					}
					break;
				case 2:
					{
					setState(83);
					subprogram_return();
					}
					break;
				}
				}
				setState(88);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(93);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==GLOBAL) {
				{
				setState(91);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
				case 1:
					{
					setState(89);
					global_assign_set();
					}
					break;
				case 2:
					{
					setState(90);
					global_assign_var();
					}
					break;
				}
				}
				setState(95);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(96);
			program();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Digit_expressionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(setLanguageParser.ID, 0); }
		public TerminalNode O_BRACKET() { return getToken(setLanguageParser.O_BRACKET, 0); }
		public List<Digit_expressionContext> digit_expression() {
			return getRuleContexts(Digit_expressionContext.class);
		}
		public Digit_expressionContext digit_expression(int i) {
			return getRuleContext(Digit_expressionContext.class,i);
		}
		public TerminalNode C_BRACKET() { return getToken(setLanguageParser.C_BRACKET, 0); }
		public TerminalNode NUMBER() { return getToken(setLanguageParser.NUMBER, 0); }
		public TerminalNode MUL() { return getToken(setLanguageParser.MUL, 0); }
		public TerminalNode DIV() { return getToken(setLanguageParser.DIV, 0); }
		public TerminalNode ADDS() { return getToken(setLanguageParser.ADDS, 0); }
		public TerminalNode SUB() { return getToken(setLanguageParser.SUB, 0); }
		public Digit_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_digit_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterDigit_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitDigit_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitDigit_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Digit_expressionContext digit_expression() throws RecognitionException {
		return digit_expression(0);
	}

	private Digit_expressionContext digit_expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Digit_expressionContext _localctx = new Digit_expressionContext(_ctx, _parentState);
		Digit_expressionContext _prevctx = _localctx;
		int _startState = 8;
		enterRecursionRule(_localctx, 8, RULE_digit_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(106);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID:
				{
				setState(100);
				match(ID);
				}
				break;
			case O_BRACKET:
				{
				setState(101);
				match(O_BRACKET);
				setState(102);
				digit_expression(0);
				setState(103);
				match(C_BRACKET);
				}
				break;
			case NUMBER:
				{
				setState(105);
				match(NUMBER);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(116);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(114);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
					case 1:
						{
						_localctx = new Digit_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_digit_expression);
						setState(108);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(109);
						_la = _input.LA(1);
						if ( !(_la==MUL || _la==DIV) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(110);
						digit_expression(6);
						}
						break;
					case 2:
						{
						_localctx = new Digit_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_digit_expression);
						setState(111);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(112);
						_la = _input.LA(1);
						if ( !(_la==ADDS || _la==SUB) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(113);
						digit_expression(5);
						}
						break;
					}
					} 
				}
				setState(118);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Intialize_setContext extends ParserRuleContext {
		public TerminalNode K_C_BRACKET() { return getToken(setLanguageParser.K_C_BRACKET, 0); }
		public TerminalNode K_O_BRACKET() { return getToken(setLanguageParser.K_O_BRACKET, 0); }
		public List<TerminalNode> ID() { return getTokens(setLanguageParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(setLanguageParser.ID, i);
		}
		public TerminalNode MUL() { return getToken(setLanguageParser.MUL, 0); }
		public TerminalNode DIV() { return getToken(setLanguageParser.DIV, 0); }
		public TerminalNode ADDS() { return getToken(setLanguageParser.ADDS, 0); }
		public TerminalNode SUB() { return getToken(setLanguageParser.SUB, 0); }
		public Intialize_setContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intialize_set; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterIntialize_set(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitIntialize_set(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitIntialize_set(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Intialize_setContext intialize_set() throws RecognitionException {
		Intialize_setContext _localctx = new Intialize_setContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_intialize_set);
		int _la;
		try {
			int _alt;
			setState(134);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_C_BRACKET:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(119);
				match(K_C_BRACKET);
				setState(120);
				match(K_O_BRACKET);
				}
				}
				break;
			case K_O_BRACKET:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(121);
				match(K_O_BRACKET);
				setState(126);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(122);
						match(ID);
						setState(123);
						match(T__1);
						}
						} 
					}
					setState(128);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
				}
				{
				setState(129);
				match(ID);
				}
				setState(130);
				match(K_C_BRACKET);
				}
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(131);
				match(ID);
				setState(132);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ADDS) | (1L << SUB) | (1L << MUL) | (1L << DIV))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(133);
				match(ID);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Intialize_elementContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(setLanguageParser.STRING, 0); }
		public Intialize_elementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intialize_element; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterIntialize_element(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitIntialize_element(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitIntialize_element(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Intialize_elementContext intialize_element() throws RecognitionException {
		Intialize_elementContext _localctx = new Intialize_elementContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_intialize_element);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(136);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assign_var_method_invocationContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(setLanguageParser.ID, 0); }
		public Method_invokationContext method_invokation() {
			return getRuleContext(Method_invokationContext.class,0);
		}
		public TerminalNode CONST() { return getToken(setLanguageParser.CONST, 0); }
		public Assign_var_method_invocationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign_var_method_invocation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterAssign_var_method_invocation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitAssign_var_method_invocation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitAssign_var_method_invocation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Assign_var_method_invocationContext assign_var_method_invocation() throws RecognitionException {
		Assign_var_method_invocationContext _localctx = new Assign_var_method_invocationContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_assign_var_method_invocation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(138);
			type();
			setState(140);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==CONST) {
				{
				setState(139);
				match(CONST);
				}
			}

			setState(142);
			match(ID);
			setState(143);
			match(T__2);
			setState(144);
			method_invokation();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assign_setContext extends ParserRuleContext {
		public TerminalNode SET() { return getToken(setLanguageParser.SET, 0); }
		public TerminalNode ID() { return getToken(setLanguageParser.ID, 0); }
		public TerminalNode SEPARATOR() { return getToken(setLanguageParser.SEPARATOR, 0); }
		public Intialize_setContext intialize_set() {
			return getRuleContext(Intialize_setContext.class,0);
		}
		public TerminalNode CONST() { return getToken(setLanguageParser.CONST, 0); }
		public Assign_setContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign_set; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterAssign_set(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitAssign_set(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitAssign_set(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Assign_setContext assign_set() throws RecognitionException {
		Assign_setContext _localctx = new Assign_setContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_assign_set);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(146);
			match(SET);
			setState(148);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==CONST) {
				{
				setState(147);
				match(CONST);
				}
			}

			setState(150);
			match(ID);
			setState(151);
			match(T__2);
			{
			setState(152);
			intialize_set();
			}
			setState(153);
			match(SEPARATOR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assign_varContext extends ParserRuleContext {
		public Type_1Context type_1() {
			return getRuleContext(Type_1Context.class,0);
		}
		public TerminalNode ID() { return getToken(setLanguageParser.ID, 0); }
		public TerminalNode SEPARATOR() { return getToken(setLanguageParser.SEPARATOR, 0); }
		public Digit_expressionContext digit_expression() {
			return getRuleContext(Digit_expressionContext.class,0);
		}
		public Intialize_elementContext intialize_element() {
			return getRuleContext(Intialize_elementContext.class,0);
		}
		public TerminalNode CONST() { return getToken(setLanguageParser.CONST, 0); }
		public Assign_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterAssign_var(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitAssign_var(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitAssign_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Assign_varContext assign_var() throws RecognitionException {
		Assign_varContext _localctx = new Assign_varContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_assign_var);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(155);
			type_1();
			setState(157);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==CONST) {
				{
				setState(156);
				match(CONST);
				}
			}

			setState(159);
			match(ID);
			setState(160);
			match(T__2);
			setState(163);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID:
			case NUMBER:
			case O_BRACKET:
				{
				setState(161);
				digit_expression(0);
				}
				break;
			case STRING:
				{
				setState(162);
				intialize_element();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(165);
			match(SEPARATOR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrintContext extends ParserRuleContext {
		public TerminalNode PRINT() { return getToken(setLanguageParser.PRINT, 0); }
		public TerminalNode O_BRACKET() { return getToken(setLanguageParser.O_BRACKET, 0); }
		public TerminalNode C_BRACKET() { return getToken(setLanguageParser.C_BRACKET, 0); }
		public TerminalNode SEPARATOR() { return getToken(setLanguageParser.SEPARATOR, 0); }
		public TerminalNode ID() { return getToken(setLanguageParser.ID, 0); }
		public TerminalNode NUMBER() { return getToken(setLanguageParser.NUMBER, 0); }
		public TerminalNode STRING() { return getToken(setLanguageParser.STRING, 0); }
		public PrintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_print; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterPrint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitPrint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitPrint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrintContext print() throws RecognitionException {
		PrintContext _localctx = new PrintContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_print);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(167);
			match(PRINT);
			setState(168);
			match(O_BRACKET);
			setState(169);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ID) | (1L << STRING) | (1L << NUMBER))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(170);
			match(C_BRACKET);
			setState(171);
			match(SEPARATOR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperationsContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(setLanguageParser.ID, 0); }
		public Digit_expressionContext digit_expression() {
			return getRuleContext(Digit_expressionContext.class,0);
		}
		public TerminalNode SEPARATOR() { return getToken(setLanguageParser.SEPARATOR, 0); }
		public OperationsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operations; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterOperations(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitOperations(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitOperations(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperationsContext operations() throws RecognitionException {
		OperationsContext _localctx = new OperationsContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_operations);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(173);
			match(ID);
			setState(174);
			match(T__2);
			setState(175);
			digit_expression(0);
			setState(176);
			match(SEPARATOR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Operarions_with_setContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(setLanguageParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(setLanguageParser.ID, i);
		}
		public TerminalNode DELIMITER() { return getToken(setLanguageParser.DELIMITER, 0); }
		public TerminalNode SEPARATOR() { return getToken(setLanguageParser.SEPARATOR, 0); }
		public TerminalNode ADD() { return getToken(setLanguageParser.ADD, 0); }
		public TerminalNode REMOVE() { return getToken(setLanguageParser.REMOVE, 0); }
		public TerminalNode O_BRACKET() { return getToken(setLanguageParser.O_BRACKET, 0); }
		public TerminalNode C_BRACKET() { return getToken(setLanguageParser.C_BRACKET, 0); }
		public Operarions_with_setContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operarions_with_set; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterOperarions_with_set(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitOperarions_with_set(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitOperarions_with_set(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Operarions_with_setContext operarions_with_set() throws RecognitionException {
		Operarions_with_setContext _localctx = new Operarions_with_setContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_operarions_with_set);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(178);
			match(ID);
			setState(179);
			match(DELIMITER);
			setState(180);
			_la = _input.LA(1);
			if ( !(_la==ADD || _la==REMOVE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			{
			setState(181);
			match(O_BRACKET);
			setState(182);
			match(ID);
			setState(183);
			match(C_BRACKET);
			}
			setState(185);
			match(SEPARATOR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Simple_compareContext extends ParserRuleContext {
		public TerminalNode EQUAL() { return getToken(setLanguageParser.EQUAL, 0); }
		public TerminalNode NON_EQUAL() { return getToken(setLanguageParser.NON_EQUAL, 0); }
		public TerminalNode LESS() { return getToken(setLanguageParser.LESS, 0); }
		public TerminalNode GREATER() { return getToken(setLanguageParser.GREATER, 0); }
		public TerminalNode LESS_OR_EQUALS() { return getToken(setLanguageParser.LESS_OR_EQUALS, 0); }
		public TerminalNode GREATER_OR_EQUALS() { return getToken(setLanguageParser.GREATER_OR_EQUALS, 0); }
		public List<Digit_expressionContext> digit_expression() {
			return getRuleContexts(Digit_expressionContext.class);
		}
		public Digit_expressionContext digit_expression(int i) {
			return getRuleContext(Digit_expressionContext.class,i);
		}
		public Simple_compareContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simple_compare; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterSimple_compare(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitSimple_compare(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitSimple_compare(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Simple_compareContext simple_compare() throws RecognitionException {
		Simple_compareContext _localctx = new Simple_compareContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_simple_compare);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(187);
			digit_expression(0);
			}
			setState(188);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUAL) | (1L << NON_EQUAL) | (1L << LESS) | (1L << LESS_OR_EQUALS) | (1L << GREATER) | (1L << GREATER_OR_EQUALS))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			{
			setState(189);
			digit_expression(0);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Hard_compareContext extends ParserRuleContext {
		public TerminalNode O_BRACKET() { return getToken(setLanguageParser.O_BRACKET, 0); }
		public Simple_compareContext simple_compare() {
			return getRuleContext(Simple_compareContext.class,0);
		}
		public TerminalNode C_BRACKET() { return getToken(setLanguageParser.C_BRACKET, 0); }
		public TerminalNode NEGATION() { return getToken(setLanguageParser.NEGATION, 0); }
		public Hard_compareContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hard_compare; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterHard_compare(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitHard_compare(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitHard_compare(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Hard_compareContext hard_compare() throws RecognitionException {
		Hard_compareContext _localctx = new Hard_compareContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_hard_compare);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==NEGATION) {
				{
				setState(191);
				match(NEGATION);
				}
			}

			setState(194);
			match(O_BRACKET);
			setState(195);
			simple_compare();
			setState(196);
			match(C_BRACKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class While_cicleContext extends ParserRuleContext {
		public TerminalNode WHILE() { return getToken(setLanguageParser.WHILE, 0); }
		public TerminalNode O_BRACKET() { return getToken(setLanguageParser.O_BRACKET, 0); }
		public TerminalNode C_BRACKET() { return getToken(setLanguageParser.C_BRACKET, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public Simple_compareContext simple_compare() {
			return getRuleContext(Simple_compareContext.class,0);
		}
		public Hard_compareContext hard_compare() {
			return getRuleContext(Hard_compareContext.class,0);
		}
		public While_cicleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_while_cicle; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterWhile_cicle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitWhile_cicle(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitWhile_cicle(this);
			else return visitor.visitChildren(this);
		}
	}

	public final While_cicleContext while_cicle() throws RecognitionException {
		While_cicleContext _localctx = new While_cicleContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_while_cicle);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(198);
			match(WHILE);
			setState(199);
			match(O_BRACKET);
			setState(202);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				{
				setState(200);
				simple_compare();
				}
				break;
			case 2:
				{
				setState(201);
				hard_compare();
				}
				break;
			}
			setState(204);
			match(C_BRACKET);
			setState(205);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_thenContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(setLanguageParser.IF, 0); }
		public TerminalNode O_BRACKET() { return getToken(setLanguageParser.O_BRACKET, 0); }
		public TerminalNode C_BRACKET() { return getToken(setLanguageParser.C_BRACKET, 0); }
		public List<BlockContext> block() {
			return getRuleContexts(BlockContext.class);
		}
		public BlockContext block(int i) {
			return getRuleContext(BlockContext.class,i);
		}
		public TerminalNode THEN() { return getToken(setLanguageParser.THEN, 0); }
		public Simple_compareContext simple_compare() {
			return getRuleContext(Simple_compareContext.class,0);
		}
		public Hard_compareContext hard_compare() {
			return getRuleContext(Hard_compareContext.class,0);
		}
		public If_thenContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_then; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterIf_then(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitIf_then(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitIf_then(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_thenContext if_then() throws RecognitionException {
		If_thenContext _localctx = new If_thenContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_if_then);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(207);
			match(IF);
			setState(208);
			match(O_BRACKET);
			setState(211);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				{
				setState(209);
				simple_compare();
				}
				break;
			case 2:
				{
				setState(210);
				hard_compare();
				}
				break;
			}
			setState(213);
			match(C_BRACKET);
			setState(214);
			block();
			setState(215);
			match(THEN);
			setState(216);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_eachContext extends ParserRuleContext {
		public TerminalNode FOR() { return getToken(setLanguageParser.FOR, 0); }
		public TerminalNode O_BRACKET() { return getToken(setLanguageParser.O_BRACKET, 0); }
		public TerminalNode ELEMENT() { return getToken(setLanguageParser.ELEMENT, 0); }
		public List<TerminalNode> ID() { return getTokens(setLanguageParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(setLanguageParser.ID, i);
		}
		public TerminalNode COLON() { return getToken(setLanguageParser.COLON, 0); }
		public TerminalNode C_BRACKET() { return getToken(setLanguageParser.C_BRACKET, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public For_eachContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_each; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterFor_each(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitFor_each(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitFor_each(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_eachContext for_each() throws RecognitionException {
		For_eachContext _localctx = new For_eachContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_for_each);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(218);
			match(FOR);
			setState(219);
			match(O_BRACKET);
			setState(220);
			match(ELEMENT);
			setState(221);
			match(ID);
			setState(222);
			match(COLON);
			setState(223);
			match(ID);
			setState(224);
			match(C_BRACKET);
			setState(225);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Global_assign_setContext extends ParserRuleContext {
		public TerminalNode GLOBAL() { return getToken(setLanguageParser.GLOBAL, 0); }
		public Assign_setContext assign_set() {
			return getRuleContext(Assign_setContext.class,0);
		}
		public Global_assign_setContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_global_assign_set; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterGlobal_assign_set(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitGlobal_assign_set(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitGlobal_assign_set(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Global_assign_setContext global_assign_set() throws RecognitionException {
		Global_assign_setContext _localctx = new Global_assign_setContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_global_assign_set);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(227);
			match(GLOBAL);
			setState(228);
			assign_set();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Global_assign_varContext extends ParserRuleContext {
		public TerminalNode GLOBAL() { return getToken(setLanguageParser.GLOBAL, 0); }
		public Assign_varContext assign_var() {
			return getRuleContext(Assign_varContext.class,0);
		}
		public Global_assign_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_global_assign_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterGlobal_assign_var(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitGlobal_assign_var(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitGlobal_assign_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Global_assign_varContext global_assign_var() throws RecognitionException {
		Global_assign_varContext _localctx = new Global_assign_varContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_global_assign_var);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(230);
			match(GLOBAL);
			setState(231);
			assign_var();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(setLanguageParser.INT, 0); }
		public TerminalNode ELEMENT() { return getToken(setLanguageParser.ELEMENT, 0); }
		public TerminalNode SET() { return getToken(setLanguageParser.SET, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(233);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SET) | (1L << ELEMENT) | (1L << INT))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_1Context extends ParserRuleContext {
		public TerminalNode INT() { return getToken(setLanguageParser.INT, 0); }
		public TerminalNode ELEMENT() { return getToken(setLanguageParser.ELEMENT, 0); }
		public Type_1Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_1; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterType_1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitType_1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitType_1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_1Context type_1() throws RecognitionException {
		Type_1Context _localctx = new Type_1Context(_ctx, getState());
		enterRule(_localctx, 42, RULE_type_1);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(235);
			_la = _input.LA(1);
			if ( !(_la==ELEMENT || _la==INT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SignatureContext extends ParserRuleContext {
		public TerminalNode O_BRACKET() { return getToken(setLanguageParser.O_BRACKET, 0); }
		public TerminalNode C_BRACKET() { return getToken(setLanguageParser.C_BRACKET, 0); }
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TerminalNode> ID() { return getTokens(setLanguageParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(setLanguageParser.ID, i);
		}
		public SignatureContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_signature; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterSignature(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitSignature(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitSignature(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SignatureContext signature() throws RecognitionException {
		SignatureContext _localctx = new SignatureContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_signature);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(237);
			match(O_BRACKET);
			setState(244);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(238);
					type();
					setState(239);
					match(ID);
					setState(240);
					match(T__1);
					}
					} 
				}
				setState(246);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			}
			{
			setState(247);
			type();
			setState(248);
			match(ID);
			}
			setState(250);
			match(C_BRACKET);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Subprogram_returnContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(setLanguageParser.ID, 0); }
		public Block_returnContext block_return() {
			return getRuleContext(Block_returnContext.class,0);
		}
		public SignatureContext signature() {
			return getRuleContext(SignatureContext.class,0);
		}
		public TerminalNode O_BRACKET() { return getToken(setLanguageParser.O_BRACKET, 0); }
		public TerminalNode C_BRACKET() { return getToken(setLanguageParser.C_BRACKET, 0); }
		public Subprogram_returnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subprogram_return; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterSubprogram_return(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitSubprogram_return(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitSubprogram_return(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Subprogram_returnContext subprogram_return() throws RecognitionException {
		Subprogram_returnContext _localctx = new Subprogram_returnContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_subprogram_return);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(252);
			match(T__3);
			setState(253);
			type();
			setState(254);
			match(ID);
			setState(258);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				{
				setState(255);
				signature();
				}
				break;
			case 2:
				{
				{
				setState(256);
				match(O_BRACKET);
				setState(257);
				match(C_BRACKET);
				}
				}
				break;
			}
			setState(260);
			block_return();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Subprogram_non_returnContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(setLanguageParser.ID, 0); }
		public SignatureContext signature() {
			return getRuleContext(SignatureContext.class,0);
		}
		public Block_non_returnContext block_non_return() {
			return getRuleContext(Block_non_returnContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode O_BRACKET() { return getToken(setLanguageParser.O_BRACKET, 0); }
		public TerminalNode C_BRACKET() { return getToken(setLanguageParser.C_BRACKET, 0); }
		public Subprogram_non_returnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subprogram_non_return; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterSubprogram_non_return(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitSubprogram_non_return(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitSubprogram_non_return(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Subprogram_non_returnContext subprogram_non_return() throws RecognitionException {
		Subprogram_non_returnContext _localctx = new Subprogram_non_returnContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_subprogram_non_return);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(262);
			match(T__3);
			setState(263);
			match(ID);
			setState(267);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
			case 1:
				{
				setState(264);
				signature();
				}
				break;
			case 2:
				{
				{
				setState(265);
				match(O_BRACKET);
				setState(266);
				match(C_BRACKET);
				}
				}
				break;
			}
			setState(271);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				{
				setState(269);
				block_non_return();
				}
				break;
			case 2:
				{
				setState(270);
				block();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Block_returnContext extends ParserRuleContext {
		public TerminalNode BEGIN() { return getToken(setLanguageParser.BEGIN, 0); }
		public TerminalNode RETURN() { return getToken(setLanguageParser.RETURN, 0); }
		public TerminalNode ID() { return getToken(setLanguageParser.ID, 0); }
		public TerminalNode SEPARATOR() { return getToken(setLanguageParser.SEPARATOR, 0); }
		public TerminalNode END() { return getToken(setLanguageParser.END, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public Block_returnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block_return; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterBlock_return(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitBlock_return(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitBlock_return(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Block_returnContext block_return() throws RecognitionException {
		Block_returnContext _localctx = new Block_returnContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_block_return);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(273);
			match(BEGIN);
			setState(277);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SET) | (1L << ELEMENT) | (1L << PRINT) | (1L << INT) | (1L << WHILE) | (1L << FOR) | (1L << IF) | (1L << CALL) | (1L << ID))) != 0)) {
				{
				{
				setState(274);
				statement();
				}
				}
				setState(279);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(280);
			match(RETURN);
			setState(281);
			match(ID);
			setState(282);
			match(SEPARATOR);
			setState(283);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Block_non_returnContext extends ParserRuleContext {
		public TerminalNode BEGIN() { return getToken(setLanguageParser.BEGIN, 0); }
		public TerminalNode RETURN() { return getToken(setLanguageParser.RETURN, 0); }
		public TerminalNode SEPARATOR() { return getToken(setLanguageParser.SEPARATOR, 0); }
		public TerminalNode END() { return getToken(setLanguageParser.END, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public Block_non_returnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block_non_return; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterBlock_non_return(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitBlock_non_return(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitBlock_non_return(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Block_non_returnContext block_non_return() throws RecognitionException {
		Block_non_returnContext _localctx = new Block_non_returnContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_block_non_return);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(285);
			match(BEGIN);
			setState(289);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SET) | (1L << ELEMENT) | (1L << PRINT) | (1L << INT) | (1L << WHILE) | (1L << FOR) | (1L << IF) | (1L << CALL) | (1L << ID))) != 0)) {
				{
				{
				setState(286);
				statement();
				}
				}
				setState(291);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(292);
			match(RETURN);
			setState(293);
			match(SEPARATOR);
			setState(294);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Signature_method_invokationContext extends ParserRuleContext {
		public TerminalNode O_BRACKET() { return getToken(setLanguageParser.O_BRACKET, 0); }
		public TerminalNode C_BRACKET() { return getToken(setLanguageParser.C_BRACKET, 0); }
		public List<TerminalNode> ID() { return getTokens(setLanguageParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(setLanguageParser.ID, i);
		}
		public Signature_method_invokationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_signature_method_invokation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterSignature_method_invokation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitSignature_method_invokation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitSignature_method_invokation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Signature_method_invokationContext signature_method_invokation() throws RecognitionException {
		Signature_method_invokationContext _localctx = new Signature_method_invokationContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_signature_method_invokation);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(296);
			match(O_BRACKET);
			setState(301);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(297);
					match(ID);
					setState(298);
					match(T__1);
					}
					} 
				}
				setState(303);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			}
			{
			setState(304);
			match(ID);
			}
			setState(305);
			match(C_BRACKET);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Method_invokationContext extends ParserRuleContext {
		public TerminalNode CALL() { return getToken(setLanguageParser.CALL, 0); }
		public TerminalNode ID() { return getToken(setLanguageParser.ID, 0); }
		public TerminalNode SEPARATOR() { return getToken(setLanguageParser.SEPARATOR, 0); }
		public Signature_method_invokationContext signature_method_invokation() {
			return getRuleContext(Signature_method_invokationContext.class,0);
		}
		public TerminalNode O_BRACKET() { return getToken(setLanguageParser.O_BRACKET, 0); }
		public TerminalNode C_BRACKET() { return getToken(setLanguageParser.C_BRACKET, 0); }
		public Method_invokationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_method_invokation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).enterMethod_invokation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof setLanguageListener ) ((setLanguageListener)listener).exitMethod_invokation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof setLanguageVisitor ) return ((setLanguageVisitor<? extends T>)visitor).visitMethod_invokation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Method_invokationContext method_invokation() throws RecognitionException {
		Method_invokationContext _localctx = new Method_invokationContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_method_invokation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(307);
			match(CALL);
			setState(308);
			match(ID);
			setState(312);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				{
				setState(309);
				signature_method_invokation();
				}
				break;
			case 2:
				{
				{
				setState(310);
				match(O_BRACKET);
				setState(311);
				match(C_BRACKET);
				}
				}
				break;
			}
			setState(314);
			match(SEPARATOR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 4:
			return digit_expression_sempred((Digit_expressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean digit_expression_sempred(Digit_expressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 5);
		case 1:
			return precpred(_ctx, 4);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3-\u013f\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\3\2\3\2\3\2\3\3\3\3"+
		"\7\3B\n\3\f\3\16\3E\13\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\5\4S\n\4\3\5\3\5\7\5W\n\5\f\5\16\5Z\13\5\3\5\3\5\7\5^\n\5\f\5\16\5"+
		"a\13\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6m\n\6\3\6\3\6\3\6\3"+
		"\6\3\6\3\6\7\6u\n\6\f\6\16\6x\13\6\3\7\3\7\3\7\3\7\3\7\7\7\177\n\7\f\7"+
		"\16\7\u0082\13\7\3\7\3\7\3\7\3\7\3\7\5\7\u0089\n\7\3\b\3\b\3\t\3\t\5\t"+
		"\u008f\n\t\3\t\3\t\3\t\3\t\3\n\3\n\5\n\u0097\n\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\13\3\13\5\13\u00a0\n\13\3\13\3\13\3\13\3\13\5\13\u00a6\n\13\3\13\3\13"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\20\5\20\u00c3\n\20\3\20\3\20"+
		"\3\20\3\20\3\21\3\21\3\21\3\21\5\21\u00cd\n\21\3\21\3\21\3\21\3\22\3\22"+
		"\3\22\3\22\5\22\u00d6\n\22\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\25\3\25\3\25\3\26\3\26\3\27"+
		"\3\27\3\30\3\30\3\30\3\30\3\30\7\30\u00f5\n\30\f\30\16\30\u00f8\13\30"+
		"\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\31\5\31\u0105\n\31"+
		"\3\31\3\31\3\32\3\32\3\32\3\32\3\32\5\32\u010e\n\32\3\32\3\32\5\32\u0112"+
		"\n\32\3\33\3\33\7\33\u0116\n\33\f\33\16\33\u0119\13\33\3\33\3\33\3\33"+
		"\3\33\3\33\3\34\3\34\7\34\u0122\n\34\f\34\16\34\u0125\13\34\3\34\3\34"+
		"\3\34\3\34\3\35\3\35\3\35\7\35\u012e\n\35\f\35\16\35\u0131\13\35\3\35"+
		"\3\35\3\35\3\36\3\36\3\36\3\36\3\36\5\36\u013b\n\36\3\36\3\36\3\36\2\3"+
		"\n\37\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:\2"+
		"\n\3\2!\"\3\2\37 \3\2\37\"\3\2\32\34\3\2\n\13\3\2$)\4\2\b\t\16\16\4\2"+
		"\t\t\16\16\2\u0145\2<\3\2\2\2\4?\3\2\2\2\6R\3\2\2\2\bX\3\2\2\2\nl\3\2"+
		"\2\2\f\u0088\3\2\2\2\16\u008a\3\2\2\2\20\u008c\3\2\2\2\22\u0094\3\2\2"+
		"\2\24\u009d\3\2\2\2\26\u00a9\3\2\2\2\30\u00af\3\2\2\2\32\u00b4\3\2\2\2"+
		"\34\u00bd\3\2\2\2\36\u00c2\3\2\2\2 \u00c8\3\2\2\2\"\u00d1\3\2\2\2$\u00dc"+
		"\3\2\2\2&\u00e5\3\2\2\2(\u00e8\3\2\2\2*\u00eb\3\2\2\2,\u00ed\3\2\2\2."+
		"\u00ef\3\2\2\2\60\u00fe\3\2\2\2\62\u0108\3\2\2\2\64\u0113\3\2\2\2\66\u011f"+
		"\3\2\2\28\u012a\3\2\2\2:\u0135\3\2\2\2<=\7\3\2\2=>\5\4\3\2>\3\3\2\2\2"+
		"?C\7\7\2\2@B\5\6\4\2A@\3\2\2\2BE\3\2\2\2CA\3\2\2\2CD\3\2\2\2DF\3\2\2\2"+
		"EC\3\2\2\2FG\7\f\2\2G\5\3\2\2\2HS\5\24\13\2IS\5\22\n\2JS\5\30\r\2KS\5"+
		"\20\t\2LS\5 \21\2MS\5\"\22\2NS\5:\36\2OS\5\26\f\2PS\5\32\16\2QS\5$\23"+
		"\2RH\3\2\2\2RI\3\2\2\2RJ\3\2\2\2RK\3\2\2\2RL\3\2\2\2RM\3\2\2\2RN\3\2\2"+
		"\2RO\3\2\2\2RP\3\2\2\2RQ\3\2\2\2S\7\3\2\2\2TW\5\62\32\2UW\5\60\31\2VT"+
		"\3\2\2\2VU\3\2\2\2WZ\3\2\2\2XV\3\2\2\2XY\3\2\2\2Y_\3\2\2\2ZX\3\2\2\2["+
		"^\5&\24\2\\^\5(\25\2][\3\2\2\2]\\\3\2\2\2^a\3\2\2\2_]\3\2\2\2_`\3\2\2"+
		"\2`b\3\2\2\2a_\3\2\2\2bc\5\2\2\2cd\b\5\1\2d\t\3\2\2\2ef\b\6\1\2fm\7\32"+
		"\2\2gh\7*\2\2hi\5\n\6\2ij\7+\2\2jm\3\2\2\2km\7\34\2\2le\3\2\2\2lg\3\2"+
		"\2\2lk\3\2\2\2mv\3\2\2\2no\f\7\2\2op\t\2\2\2pu\5\n\6\bqr\f\6\2\2rs\t\3"+
		"\2\2su\5\n\6\7tn\3\2\2\2tq\3\2\2\2ux\3\2\2\2vt\3\2\2\2vw\3\2\2\2w\13\3"+
		"\2\2\2xv\3\2\2\2yz\7-\2\2z\u0089\7,\2\2{\u0080\7,\2\2|}\7\32\2\2}\177"+
		"\7\4\2\2~|\3\2\2\2\177\u0082\3\2\2\2\u0080~\3\2\2\2\u0080\u0081\3\2\2"+
		"\2\u0081\u0083\3\2\2\2\u0082\u0080\3\2\2\2\u0083\u0084\7\32\2\2\u0084"+
		"\u0089\7-\2\2\u0085\u0086\7\32\2\2\u0086\u0087\t\4\2\2\u0087\u0089\7\32"+
		"\2\2\u0088y\3\2\2\2\u0088{\3\2\2\2\u0088\u0085\3\2\2\2\u0089\r\3\2\2\2"+
		"\u008a\u008b\7\33\2\2\u008b\17\3\2\2\2\u008c\u008e\5*\26\2\u008d\u008f"+
		"\7\21\2\2\u008e\u008d\3\2\2\2\u008e\u008f\3\2\2\2\u008f\u0090\3\2\2\2"+
		"\u0090\u0091\7\32\2\2\u0091\u0092\7\5\2\2\u0092\u0093\5:\36\2\u0093\21"+
		"\3\2\2\2\u0094\u0096\7\b\2\2\u0095\u0097\7\21\2\2\u0096\u0095\3\2\2\2"+
		"\u0096\u0097\3\2\2\2\u0097\u0098\3\2\2\2\u0098\u0099\7\32\2\2\u0099\u009a"+
		"\7\5\2\2\u009a\u009b\5\f\7\2\u009b\u009c\7\26\2\2\u009c\23\3\2\2\2\u009d"+
		"\u009f\5,\27\2\u009e\u00a0\7\21\2\2\u009f\u009e\3\2\2\2\u009f\u00a0\3"+
		"\2\2\2\u00a0\u00a1\3\2\2\2\u00a1\u00a2\7\32\2\2\u00a2\u00a5\7\5\2\2\u00a3"+
		"\u00a6\5\n\6\2\u00a4\u00a6\5\16\b\2\u00a5\u00a3\3\2\2\2\u00a5\u00a4\3"+
		"\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\u00a8\7\26\2\2\u00a8\25\3\2\2\2\u00a9"+
		"\u00aa\7\r\2\2\u00aa\u00ab\7*\2\2\u00ab\u00ac\t\5\2\2\u00ac\u00ad\7+\2"+
		"\2\u00ad\u00ae\7\26\2\2\u00ae\27\3\2\2\2\u00af\u00b0\7\32\2\2\u00b0\u00b1"+
		"\7\5\2\2\u00b1\u00b2\5\n\6\2\u00b2\u00b3\7\26\2\2\u00b3\31\3\2\2\2\u00b4"+
		"\u00b5\7\32\2\2\u00b5\u00b6\7\27\2\2\u00b6\u00b7\t\6\2\2\u00b7\u00b8\7"+
		"*\2\2\u00b8\u00b9\7\32\2\2\u00b9\u00ba\7+\2\2\u00ba\u00bb\3\2\2\2\u00bb"+
		"\u00bc\7\26\2\2\u00bc\33\3\2\2\2\u00bd\u00be\5\n\6\2\u00be\u00bf\t\7\2"+
		"\2\u00bf\u00c0\5\n\6\2\u00c0\35\3\2\2\2\u00c1\u00c3\7#\2\2\u00c2\u00c1"+
		"\3\2\2\2\u00c2\u00c3\3\2\2\2\u00c3\u00c4\3\2\2\2\u00c4\u00c5\7*\2\2\u00c5"+
		"\u00c6\5\34\17\2\u00c6\u00c7\7+\2\2\u00c7\37\3\2\2\2\u00c8\u00c9\7\17"+
		"\2\2\u00c9\u00cc\7*\2\2\u00ca\u00cd\5\34\17\2\u00cb\u00cd\5\36\20\2\u00cc"+
		"\u00ca\3\2\2\2\u00cc\u00cb\3\2\2\2\u00cd\u00ce\3\2\2\2\u00ce\u00cf\7+"+
		"\2\2\u00cf\u00d0\5\4\3\2\u00d0!\3\2\2\2\u00d1\u00d2\7\22\2\2\u00d2\u00d5"+
		"\7*\2\2\u00d3\u00d6\5\34\17\2\u00d4\u00d6\5\36\20\2\u00d5\u00d3\3\2\2"+
		"\2\u00d5\u00d4\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7\u00d8\7+\2\2\u00d8\u00d9"+
		"\5\4\3\2\u00d9\u00da\7\23\2\2\u00da\u00db\5\4\3\2\u00db#\3\2\2\2\u00dc"+
		"\u00dd\7\20\2\2\u00dd\u00de\7*\2\2\u00de\u00df\7\t\2\2\u00df\u00e0\7\32"+
		"\2\2\u00e0\u00e1\7\30\2\2\u00e1\u00e2\7\32\2\2\u00e2\u00e3\7+\2\2\u00e3"+
		"\u00e4\5\4\3\2\u00e4%\3\2\2\2\u00e5\u00e6\7\31\2\2\u00e6\u00e7\5\22\n"+
		"\2\u00e7\'\3\2\2\2\u00e8\u00e9\7\31\2\2\u00e9\u00ea\5\24\13\2\u00ea)\3"+
		"\2\2\2\u00eb\u00ec\t\b\2\2\u00ec+\3\2\2\2\u00ed\u00ee\t\t\2\2\u00ee-\3"+
		"\2\2\2\u00ef\u00f6\7*\2\2\u00f0\u00f1\5*\26\2\u00f1\u00f2\7\32\2\2\u00f2"+
		"\u00f3\7\4\2\2\u00f3\u00f5\3\2\2\2\u00f4\u00f0\3\2\2\2\u00f5\u00f8\3\2"+
		"\2\2\u00f6\u00f4\3\2\2\2\u00f6\u00f7\3\2\2\2\u00f7\u00f9\3\2\2\2\u00f8"+
		"\u00f6\3\2\2\2\u00f9\u00fa\5*\26\2\u00fa\u00fb\7\32\2\2\u00fb\u00fc\3"+
		"\2\2\2\u00fc\u00fd\7+\2\2\u00fd/\3\2\2\2\u00fe\u00ff\7\6\2\2\u00ff\u0100"+
		"\5*\26\2\u0100\u0104\7\32\2\2\u0101\u0105\5.\30\2\u0102\u0103\7*\2\2\u0103"+
		"\u0105\7+\2\2\u0104\u0101\3\2\2\2\u0104\u0102\3\2\2\2\u0105\u0106\3\2"+
		"\2\2\u0106\u0107\5\64\33\2\u0107\61\3\2\2\2\u0108\u0109\7\6\2\2\u0109"+
		"\u010d\7\32\2\2\u010a\u010e\5.\30\2\u010b\u010c\7*\2\2\u010c\u010e\7+"+
		"\2\2\u010d\u010a\3\2\2\2\u010d\u010b\3\2\2\2\u010e\u0111\3\2\2\2\u010f"+
		"\u0112\5\66\34\2\u0110\u0112\5\4\3\2\u0111\u010f\3\2\2\2\u0111\u0110\3"+
		"\2\2\2\u0112\63\3\2\2\2\u0113\u0117\7\7\2\2\u0114\u0116\5\6\4\2\u0115"+
		"\u0114\3\2\2\2\u0116\u0119\3\2\2\2\u0117\u0115\3\2\2\2\u0117\u0118\3\2"+
		"\2\2\u0118\u011a\3\2\2\2\u0119\u0117\3\2\2\2\u011a\u011b\7\25\2\2\u011b"+
		"\u011c\7\32\2\2\u011c\u011d\7\26\2\2\u011d\u011e\7\f\2\2\u011e\65\3\2"+
		"\2\2\u011f\u0123\7\7\2\2\u0120\u0122\5\6\4\2\u0121\u0120\3\2\2\2\u0122"+
		"\u0125\3\2\2\2\u0123\u0121\3\2\2\2\u0123\u0124\3\2\2\2\u0124\u0126\3\2"+
		"\2\2\u0125\u0123\3\2\2\2\u0126\u0127\7\25\2\2\u0127\u0128\7\26\2\2\u0128"+
		"\u0129\7\f\2\2\u0129\67\3\2\2\2\u012a\u012f\7*\2\2\u012b\u012c\7\32\2"+
		"\2\u012c\u012e\7\4\2\2\u012d\u012b\3\2\2\2\u012e\u0131\3\2\2\2\u012f\u012d"+
		"\3\2\2\2\u012f\u0130\3\2\2\2\u0130\u0132\3\2\2\2\u0131\u012f\3\2\2\2\u0132"+
		"\u0133\7\32\2\2\u0133\u0134\7+\2\2\u01349\3\2\2\2\u0135\u0136\7\24\2\2"+
		"\u0136\u013a\7\32\2\2\u0137\u013b\58\35\2\u0138\u0139\7*\2\2\u0139\u013b"+
		"\7+\2\2\u013a\u0137\3\2\2\2\u013a\u0138\3\2\2\2\u013b\u013c\3\2\2\2\u013c"+
		"\u013d\7\26\2\2\u013d;\3\2\2\2\34CRVX]_ltv\u0080\u0088\u008e\u0096\u009f"+
		"\u00a5\u00c2\u00cc\u00d5\u00f6\u0104\u010d\u0111\u0117\u0123\u012f\u013a";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}