package gen;// Generated from D:/Programs/IntelliJ IDEA 2019.3.4/proj/setCompiler\setLanguage.g4 by ANTLR 4.8
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class setLanguageLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, BEGIN=5, SET=6, ELEMENT=7, ADD=8, REMOVE=9, 
		END=10, PRINT=11, INT=12, WHILE=13, FOR=14, CONST=15, IF=16, THEN=17, 
		CALL=18, RETURN=19, SEPARATOR=20, DELIMITER=21, COLON=22, GLOBAL=23, ID=24, 
		STRING=25, NUMBER=26, WS=27, COMMENTS=28, ADDS=29, SUB=30, MUL=31, DIV=32, 
		NEGATION=33, EQUAL=34, NON_EQUAL=35, LESS=36, LESS_OR_EQUALS=37, GREATER=38, 
		GREATER_OR_EQUALS=39, O_BRACKET=40, C_BRACKET=41, K_O_BRACKET=42, K_C_BRACKET=43;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"T__0", "T__1", "T__2", "T__3", "BEGIN", "SET", "ELEMENT", "ADD", "REMOVE", 
			"END", "PRINT", "INT", "WHILE", "FOR", "CONST", "IF", "THEN", "CALL", 
			"RETURN", "SEPARATOR", "DELIMITER", "COLON", "GLOBAL", "ID", "STRING", 
			"NUMBER", "WS", "COMMENTS", "ADDS", "SUB", "MUL", "DIV", "NEGATION", 
			"EQUAL", "NON_EQUAL", "LESS", "LESS_OR_EQUALS", "GREATER", "GREATER_OR_EQUALS", 
			"O_BRACKET", "C_BRACKET", "K_O_BRACKET", "K_C_BRACKET"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'main_program'", "','", "'='", "'sub_program'", "'begin'", "'set'", 
			"'element'", "'add'", "'remove'", "'end'", "'print'", "'int'", "'while'", 
			"'for'", "'const'", "'if'", "'then'", "'call'", "'return'", "';'", "'.'", 
			"':'", "'global'", null, null, null, null, null, "'+'", "'-'", "'*'", 
			"'/'", "'!'", "'=='", "'!='", "'<'", "'<='", "'>'", "'>='", "'('", "')'", 
			"'{'", "'}'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, "BEGIN", "SET", "ELEMENT", "ADD", "REMOVE", 
			"END", "PRINT", "INT", "WHILE", "FOR", "CONST", "IF", "THEN", "CALL", 
			"RETURN", "SEPARATOR", "DELIMITER", "COLON", "GLOBAL", "ID", "STRING", 
			"NUMBER", "WS", "COMMENTS", "ADDS", "SUB", "MUL", "DIV", "NEGATION", 
			"EQUAL", "NON_EQUAL", "LESS", "LESS_OR_EQUALS", "GREATER", "GREATER_OR_EQUALS", 
			"O_BRACKET", "C_BRACKET", "K_O_BRACKET", "K_C_BRACKET"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public setLanguageLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "setLanguage.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2-\u011a\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3"+
		"\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3"+
		"\n\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\23"+
		"\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\26"+
		"\3\26\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\31\3\31\7\31\u00d5"+
		"\n\31\f\31\16\31\u00d8\13\31\3\32\3\32\6\32\u00dc\n\32\r\32\16\32\u00dd"+
		"\3\32\3\32\3\33\6\33\u00e3\n\33\r\33\16\33\u00e4\3\34\6\34\u00e8\n\34"+
		"\r\34\16\34\u00e9\3\34\3\34\3\35\3\35\3\35\3\35\7\35\u00f2\n\35\f\35\16"+
		"\35\u00f5\13\35\3\35\3\35\3\36\3\36\3\37\3\37\3 \3 \3!\3!\3\"\3\"\3#\3"+
		"#\3#\3$\3$\3$\3%\3%\3&\3&\3&\3\'\3\'\3(\3(\3(\3)\3)\3*\3*\3+\3+\3,\3,"+
		"\3\u00dd\2-\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33"+
		"\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67"+
		"\359\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-\3\2\7\5\2C\\aac|\6\2\62;C\\"+
		"aac|\3\2\62;\5\2\13\f\17\17\"\"\4\2\f\f\17\17\2\u011e\2\3\3\2\2\2\2\5"+
		"\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2"+
		"\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33"+
		"\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2"+
		"\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2"+
		"\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2"+
		"\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K"+
		"\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2"+
		"\2\2\3Y\3\2\2\2\5f\3\2\2\2\7h\3\2\2\2\tj\3\2\2\2\13v\3\2\2\2\r|\3\2\2"+
		"\2\17\u0080\3\2\2\2\21\u0088\3\2\2\2\23\u008c\3\2\2\2\25\u0093\3\2\2\2"+
		"\27\u0097\3\2\2\2\31\u009d\3\2\2\2\33\u00a1\3\2\2\2\35\u00a7\3\2\2\2\37"+
		"\u00ab\3\2\2\2!\u00b1\3\2\2\2#\u00b4\3\2\2\2%\u00b9\3\2\2\2\'\u00be\3"+
		"\2\2\2)\u00c5\3\2\2\2+\u00c7\3\2\2\2-\u00c9\3\2\2\2/\u00cb\3\2\2\2\61"+
		"\u00d2\3\2\2\2\63\u00d9\3\2\2\2\65\u00e2\3\2\2\2\67\u00e7\3\2\2\29\u00ed"+
		"\3\2\2\2;\u00f8\3\2\2\2=\u00fa\3\2\2\2?\u00fc\3\2\2\2A\u00fe\3\2\2\2C"+
		"\u0100\3\2\2\2E\u0102\3\2\2\2G\u0105\3\2\2\2I\u0108\3\2\2\2K\u010a\3\2"+
		"\2\2M\u010d\3\2\2\2O\u010f\3\2\2\2Q\u0112\3\2\2\2S\u0114\3\2\2\2U\u0116"+
		"\3\2\2\2W\u0118\3\2\2\2YZ\7o\2\2Z[\7c\2\2[\\\7k\2\2\\]\7p\2\2]^\7a\2\2"+
		"^_\7r\2\2_`\7t\2\2`a\7q\2\2ab\7i\2\2bc\7t\2\2cd\7c\2\2de\7o\2\2e\4\3\2"+
		"\2\2fg\7.\2\2g\6\3\2\2\2hi\7?\2\2i\b\3\2\2\2jk\7u\2\2kl\7w\2\2lm\7d\2"+
		"\2mn\7a\2\2no\7r\2\2op\7t\2\2pq\7q\2\2qr\7i\2\2rs\7t\2\2st\7c\2\2tu\7"+
		"o\2\2u\n\3\2\2\2vw\7d\2\2wx\7g\2\2xy\7i\2\2yz\7k\2\2z{\7p\2\2{\f\3\2\2"+
		"\2|}\7u\2\2}~\7g\2\2~\177\7v\2\2\177\16\3\2\2\2\u0080\u0081\7g\2\2\u0081"+
		"\u0082\7n\2\2\u0082\u0083\7g\2\2\u0083\u0084\7o\2\2\u0084\u0085\7g\2\2"+
		"\u0085\u0086\7p\2\2\u0086\u0087\7v\2\2\u0087\20\3\2\2\2\u0088\u0089\7"+
		"c\2\2\u0089\u008a\7f\2\2\u008a\u008b\7f\2\2\u008b\22\3\2\2\2\u008c\u008d"+
		"\7t\2\2\u008d\u008e\7g\2\2\u008e\u008f\7o\2\2\u008f\u0090\7q\2\2\u0090"+
		"\u0091\7x\2\2\u0091\u0092\7g\2\2\u0092\24\3\2\2\2\u0093\u0094\7g\2\2\u0094"+
		"\u0095\7p\2\2\u0095\u0096\7f\2\2\u0096\26\3\2\2\2\u0097\u0098\7r\2\2\u0098"+
		"\u0099\7t\2\2\u0099\u009a\7k\2\2\u009a\u009b\7p\2\2\u009b\u009c\7v\2\2"+
		"\u009c\30\3\2\2\2\u009d\u009e\7k\2\2\u009e\u009f\7p\2\2\u009f\u00a0\7"+
		"v\2\2\u00a0\32\3\2\2\2\u00a1\u00a2\7y\2\2\u00a2\u00a3\7j\2\2\u00a3\u00a4"+
		"\7k\2\2\u00a4\u00a5\7n\2\2\u00a5\u00a6\7g\2\2\u00a6\34\3\2\2\2\u00a7\u00a8"+
		"\7h\2\2\u00a8\u00a9\7q\2\2\u00a9\u00aa\7t\2\2\u00aa\36\3\2\2\2\u00ab\u00ac"+
		"\7e\2\2\u00ac\u00ad\7q\2\2\u00ad\u00ae\7p\2\2\u00ae\u00af\7u\2\2\u00af"+
		"\u00b0\7v\2\2\u00b0 \3\2\2\2\u00b1\u00b2\7k\2\2\u00b2\u00b3\7h\2\2\u00b3"+
		"\"\3\2\2\2\u00b4\u00b5\7v\2\2\u00b5\u00b6\7j\2\2\u00b6\u00b7\7g\2\2\u00b7"+
		"\u00b8\7p\2\2\u00b8$\3\2\2\2\u00b9\u00ba\7e\2\2\u00ba\u00bb\7c\2\2\u00bb"+
		"\u00bc\7n\2\2\u00bc\u00bd\7n\2\2\u00bd&\3\2\2\2\u00be\u00bf\7t\2\2\u00bf"+
		"\u00c0\7g\2\2\u00c0\u00c1\7v\2\2\u00c1\u00c2\7w\2\2\u00c2\u00c3\7t\2\2"+
		"\u00c3\u00c4\7p\2\2\u00c4(\3\2\2\2\u00c5\u00c6\7=\2\2\u00c6*\3\2\2\2\u00c7"+
		"\u00c8\7\60\2\2\u00c8,\3\2\2\2\u00c9\u00ca\7<\2\2\u00ca.\3\2\2\2\u00cb"+
		"\u00cc\7i\2\2\u00cc\u00cd\7n\2\2\u00cd\u00ce\7q\2\2\u00ce\u00cf\7d\2\2"+
		"\u00cf\u00d0\7c\2\2\u00d0\u00d1\7n\2\2\u00d1\60\3\2\2\2\u00d2\u00d6\t"+
		"\2\2\2\u00d3\u00d5\t\3\2\2\u00d4\u00d3\3\2\2\2\u00d5\u00d8\3\2\2\2\u00d6"+
		"\u00d4\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7\62\3\2\2\2\u00d8\u00d6\3\2\2"+
		"\2\u00d9\u00db\7$\2\2\u00da\u00dc\13\2\2\2\u00db\u00da\3\2\2\2\u00dc\u00dd"+
		"\3\2\2\2\u00dd\u00de\3\2\2\2\u00dd\u00db\3\2\2\2\u00de\u00df\3\2\2\2\u00df"+
		"\u00e0\7$\2\2\u00e0\64\3\2\2\2\u00e1\u00e3\t\4\2\2\u00e2\u00e1\3\2\2\2"+
		"\u00e3\u00e4\3\2\2\2\u00e4\u00e2\3\2\2\2\u00e4\u00e5\3\2\2\2\u00e5\66"+
		"\3\2\2\2\u00e6\u00e8\t\5\2\2\u00e7\u00e6\3\2\2\2\u00e8\u00e9\3\2\2\2\u00e9"+
		"\u00e7\3\2\2\2\u00e9\u00ea\3\2\2\2\u00ea\u00eb\3\2\2\2\u00eb\u00ec\b\34"+
		"\2\2\u00ec8\3\2\2\2\u00ed\u00ee\7\61\2\2\u00ee\u00ef\7\61\2\2\u00ef\u00f3"+
		"\3\2\2\2\u00f0\u00f2\n\6\2\2\u00f1\u00f0\3\2\2\2\u00f2\u00f5\3\2\2\2\u00f3"+
		"\u00f1\3\2\2\2\u00f3\u00f4\3\2\2\2\u00f4\u00f6\3\2\2\2\u00f5\u00f3\3\2"+
		"\2\2\u00f6\u00f7\b\35\2\2\u00f7:\3\2\2\2\u00f8\u00f9\7-\2\2\u00f9<\3\2"+
		"\2\2\u00fa\u00fb\7/\2\2\u00fb>\3\2\2\2\u00fc\u00fd\7,\2\2\u00fd@\3\2\2"+
		"\2\u00fe\u00ff\7\61\2\2\u00ffB\3\2\2\2\u0100\u0101\7#\2\2\u0101D\3\2\2"+
		"\2\u0102\u0103\7?\2\2\u0103\u0104\7?\2\2\u0104F\3\2\2\2\u0105\u0106\7"+
		"#\2\2\u0106\u0107\7?\2\2\u0107H\3\2\2\2\u0108\u0109\7>\2\2\u0109J\3\2"+
		"\2\2\u010a\u010b\7>\2\2\u010b\u010c\7?\2\2\u010cL\3\2\2\2\u010d\u010e"+
		"\7@\2\2\u010eN\3\2\2\2\u010f\u0110\7@\2\2\u0110\u0111\7?\2\2\u0111P\3"+
		"\2\2\2\u0112\u0113\7*\2\2\u0113R\3\2\2\2\u0114\u0115\7+\2\2\u0115T\3\2"+
		"\2\2\u0116\u0117\7}\2\2\u0117V\3\2\2\2\u0118\u0119\7\177\2\2\u0119X\3"+
		"\2\2\2\b\2\u00d6\u00dd\u00e4\u00e9\u00f3\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}