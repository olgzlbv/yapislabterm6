# YAPISLabTerm6
 Компилятор языка множеств

# Условие по лабораторной работе:
Язык(4-язык множеств)
Целевой код(Java)
Свойства(16)

# Описание лабораторной работы
Данное приложение парсит файлы с расширением .mn, в которых прописан код на языке множеств соответствующего свойствам выбранного варианта(16)
На выходе программой будет сгенерирован файл Main.java с java кодом в папку generated.В данной папке так же хранятся встроенные типы данных set и element.


# Алфавит выбранного языка(4-язык множеств)
Файл с грамматикой находится в корне проекта setLanguage.g4

# Пример работы программы

## set.mn - входные данные
```
//Блочные операторы: begin end
//Встроенные типы: set, element

sub_program int sub(int b) // Объявление подпрограммы в начале программы
    begin
        print(b);
        return b;
    end

global int d = 5;  // Объявление глобальной переменной

main_program
    begin
        print(1);
        int a = 1 ; // Объявление переменной
        int const b = call sub(a); // Вызов подпрограммы и инициализация константы
        element v = "abc"; //Инициализация элемента
        set c = {v, v}; //Инициализация множества
        set kk = }{;  //Инициализация пустого множества
        c.remove(v);
        while (a < 3)
            begin
            a = a + 1;
            end
        if (a == 3)
            begin
                print("3, hi");
            end
        then
            begin
            end

    end
```

## Сгенерированный Main.java - результат
```java
public class Main {
public static void main (String args[]){
try{
main_program();} catch (Exception ex){ex.printStackTrace();}
}
private static int d=5;
private static int sub(int b){
System.out.println(String.valueOf(b));
return b;
}
private static void main_program () {
System.out.println(String.valueOf(1));
int a=1;
int b=sub (a);
Element v=new Element("abc");
Set c=new Set();
c.add(v);
c.add(v);
Set kk=new Set();
c.remove(v);
Set h=Set.union(c,c);
System.out.println(String.valueOf(c));
for (Element e:h){
System.out.println(String.valueOf(e));
}
while (a<3){
a=a+1;
}
if (a==3) {
System.out.println(String.valueOf("3, hi"));
}
 else {
}
}

}
```
>>>>>>> add repo
}
}

}
```
>>>>>>> add repo
